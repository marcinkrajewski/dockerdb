FROM postgres:14.0

ENV POSTGRES_PASSWORD password
ENV POSTGRES_DB hepatitis

COPY schema/* /docker-entrypoint-initdb.d/