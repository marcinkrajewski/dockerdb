-- liquibase formatted sql

-- changeset mkrajewski:1636930876311-1
CREATE TABLE "public"."COVID_LABOLATORY_DIC" ("ID" BIGINT, "NAZWA" VARCHAR(500), "ID_WOJEWODZTWO" BIGINT, "WOJEWODZTWO" VARCHAR(100), "ID_POWIAT" BIGINT, "POWIAT" VARCHAR(100), "ID_GMINA" BIGINT, "GMINA" VARCHAR(100), "ID_MIEJSCOWOSC" BIGINT, "MIEJSCOWOSC" VARCHAR(100), "ID_ULICA" BIGINT, "ULICA" VARCHAR(255), "NR_DOMU" VARCHAR(150), "NR_LOKALU" VARCHAR(150), "KOD_POCZTOWY" VARCHAR(100), "POJEMNOSC" BIGINT, "AKTYWNY" VARCHAR(20), "KOD_ODDZIALU_NFZ" BIGINT, "CZY_UMOWA_NFZ" VARCHAR(20), "ID_PODMIOTU" BIGINT, "AUTH_KEY" VARCHAR(255), "CRUIP_KEY" VARCHAR(255), "EMAIL" VARCHAR(255));

