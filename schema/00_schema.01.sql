-- liquibase formatted sql

-- changeset mkrajewski:1636929969062-1
CREATE TABLE "public"."hera_user" ("ID" BIGINT, "SOFT_DEL" BOOLEAN, "USER_NAME" VARCHAR(50), "NAME" VARCHAR(50), "LAST_NAME" VARCHAR(150), "EMAIL" VARCHAR(100), "DATE_CREATE" date, "DATE_FROM" date, "DATE_TO" date, "DEPARTMENT_ID" BIGINT, "ACTIVE" BOOLEAN DEFAULT TRUE, "EXT" BOOLEAN);

-- changeset mkrajewski:1636929969062-2
CREATE TABLE "public"."hera_permission" ("ID" BIGINT, "PERMISSION" VARCHAR(255));

-- changeset mkrajewski:1636929969062-3
CREATE TABLE "public"."hera_role" ("ID" BIGINT, "SOFT_DEL" BOOLEAN, "NAME" VARCHAR(50), "DATE_FROM" date, "DATE_TO" date, "DESCRIPTION" VARCHAR(255));

-- changeset mkrajewski:1636929969062-4
CREATE TABLE "public"."hera_user_role" ("USER_ID" BIGINT, "ROLE_ID" BIGINT);

